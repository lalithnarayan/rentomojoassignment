(function() {
    'use strict';

    angular
        .module('myApp')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($scope,$http) {
        $scope.showLoader = true;

        $scope.listing =[{
            "name":"Peter England Cap",
            "image":"../assets/images/cap1.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        },
        {
            "name":"Peter England Cap",
            "image":"../assets/images/cap3.jpg",
            "type":"normal",
            "oldPrice":120,
            "NewPrice":90
        },
        {
            "name":"Peter England Cap",
            "image":"../assets/images/shirt1.jpg",
            "type":"hit",
            "oldPrice":120,
            "NewPrice":90
        },{
            "name":"Peter England Cap",
            "image":"../assets/images/shirt2.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        },{
            "name":"Peter England Cap",
            "image":"../assets/images/shirt3.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        },{
            "name":"Peter England Cap",
            "image":"../assets/images/shirt1.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        },{
            "name":"Peter England Cap",
            "image":"../assets/images/shirt2.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        },{
            "name":"Peter England Cap",
            "image":"../assets/images/shirt3.jpg",
            "type":"sale",
            "oldPrice":120,
            "NewPrice":90
        }];

        $scope.sidePanelImages = [{"image":"../assets/images/side1.jpg"},{"image":"../assets/images/side2.jpg"},{"image":"../assets/images/side3.jpg"},{"image":"../assets/images/side4.jpg"}]
        $scope.brands=[{"image":"../assets/images/logo2.jpeg"},{"image":"../assets/images/logo3.png"},{"image":"../assets/images/logo4.png"},{"image":"../assets/images/logo4.png"},{"image":"../assets/images/logo4.png"},{"image":"../assets/images/logo2.jpeg"},{"image":"../assets/images/logo3.png"}]
        $scope.socialLogos= [{"image":"../assets/images/twitter.png"},{"image":"../assets/images/insta.png"},{"image":"../assets/images/pin.png"},{"image":"../assets/images/fb.png"}];
        $scope.payLogos= [{"image":"../assets/images/visa.png"},{"image":"../assets/images/master.png"},{"image":"../assets/images/paypal.png"}];
        $scope.bannerImages= [{"image":"../assets/images/1.jpg"},{"image":"../assets/images/2.jpg"},{"image":"../assets/images/3.jpg"}]

        $scope.getBrandImageBg = function(imagePath){
            return{
                'background-image':'url('+imagePath+')'
            }
        }
    }
})();
